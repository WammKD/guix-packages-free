(define-module
  (pkill9 packages oguri)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system meson)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages freedesktop))

(define-public oguri
  (package
    (name "oguri")
    (version "git")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/vilhalmer/oguri")
               (recursive? #t)
               (commit "abf26bf2646d00d91b27cf0336cca38d17057bed")))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1bkd448d6359cylwjar7xnlpic3bvzc650a7fbdcc11l1jw21gq5"))))
    (build-system meson-build-system)
    (native-inputs `(("pkg-config" ,pkg-config)))
    (inputs
      `(("cairo" ,cairo)
        ("gdk-pixbuf+svg" ,gdk-pixbuf+svg)
        ("wayland" ,wayland)
        ("wayland-protocols" ,wayland-protocols)))
    (home-page "https://github.com/vilhalmer/oguri")
    (synopsis "Animated wallpaper daemon for Wayland compositors")
    (description "Animated wallpaper daemon for Wayland compositors")
    (license "license:expat")))

oguri
