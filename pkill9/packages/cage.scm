(define-module (pkill9 packages cage)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system meson)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages build-tools)
  #:use-module (gnu packages wm)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages gl)
  #:use-module (srfi srfi-1))

;;wlroots-next and meson-for-build-next not needed afterall.. just leave here cos i worked on this
(define-public meson-for-build-next
  (package (inherit meson-for-build)
    (version "0.53.1")
    (source (origin (inherit (package-source meson-for-build))
              (method url-fetch)
              (uri (string-append "https://github.com/mesonbuild/meson/"
                                  "releases/download/" version  "/meson-"
                                  version ".tar.gz"))
              (sha256
               (base32
                "011v84ijdnmbdgpzslaa6qiq7p2jh52sqzb0q6iaq6vhx8za66zc"))))))

(define-public wlroots-next
  (package (inherit wlroots)
   (name "wlroots-next")
   (version "0.10.0")
   (source (origin
            (method url-fetch)
            (uri "https://github.com/swaywm/wlroots/releases/download/0.10.0/wlroots-0.10.0.tar.gz")
            
            (sha256
             (base32
              "1kgn2kbdvl862wyn8ikxz6l3yi2mxnxy29h47qmrq7rj3ivbl54l"))))
   (arguments
    `(,@(package-arguments wlroots)
      #:meson ,meson-for-build-next))))

(define-public cage
  (package
   (name "cage")
   (version "0.1.1")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://github.com/Hjdskes/cage.git")
                  (commit (string-append "v" version))))
            (file-name (git-file-name name version))
            (snippet
             '(begin
                (use-modules (guix build utils))
                (substitute* "meson.build"
                             (("warning_level=3") "warning_level=0")
                             (("werror=true") "werror=false"))))
            (sha256
             (base32
              "1vp4mfkflrjmlgyx5mkbzdi3iq58m76q7l9dfrsk85xn0642d6q1"))))
   (build-system meson-build-system)
   (native-inputs (package-native-inputs sway)) ;; doesn't actually need al the inputs that sway needs, I'm just lazy
    (inputs `(("elogind" ,elogind)
              ("eudev" ,eudev)
              ("libinput" ,libinput)
              ("libxkbcommon" ,libxkbcommon)
              ("mesa" ,mesa)
              ("pixman" ,pixman)
              ("wayland" ,wayland)
              ("wlroots" ,wlroots)))
;;   (arguments
;;    `(#:configure-flags '("-Dxwayland=true")))
   (home-page "https://github.com/mesonbuild/meson")
   (synopsis "Kiosk Wayland compositor")
   (description "")
   (license license:expat)))

cage
