(define-module (pkill9 packages mypaint)
  #:use-module (guix download)
  #:use-module (guix build-system scons)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages swig)
  #:use-module (gnu packages image)
  #:use-module (gnu packages ghostscript) ; for lcms
  #:use-module (gnu packages web) ;for json-c
  #:use-module (gnu packages python) ;for python-2
  #:use-module (gnu packages python-xyz) ;for python2-numpy
  #:use-module (gnu packages glib) ;for python-pygobject
  #:use-module (gnu packages gnome) ;for hicolor-icon-theme
  )

(define-public mypaint
  (package
   (name "mypaint")
   (version "1.2.1")
   (source (origin
	    (method url-fetch)
	    (uri (string-append "https://github.com/mypaint/mypaint/releases/download/v" version "/mypaint-" version ".tar.xz"))
	    
	    (sha256
	     (base32
	      "1iykjvfis5y71wn9wyxpmha7vb4pgr10dbpgglymh33a5ww4j8zd"))))
   (build-system scons-build-system)
   (native-inputs
    `(("pkg-config" ,pkg-config)
      ("gettext" ,gettext-minimal)

      ("swig" ,swig))) ; without swig, fails during build with `scons: *** [_mypaintlib.so] Source file: lib/mypaintlib.i is static and is not compatible with shared target: _mypaintlib.so`
   (inputs
    `(;("libpng" ,libpng) ; seems to compile and run fine without this, but the README_LINUX.md file says it's a requirement... I'll leave it out and see if anything goes wrong without it.
      ("lcms" ,lcms)
      ("json-c" ,json-c)))
   (propagated-inputs ; python wants these
    `(("python2-numpy" ,python2-numpy)
      ("python-2" ,python-2) ; Adding this creates the PYTHONPATH variable with necessary paths
      ("python2-pygobject" ,python2-pygobject)
      ("python2-pycairo" ,python2-pycairo)
      ("gtk+" ,gtk+)
      ("gdk-pixbuf" ,gdk-pixbuf)
      ("hicolor-icon-theme" ,hicolor-icon-theme)
      ))
   (arguments
    `(#:tests? #f ; Fails to find test target, but there are tests
      #:scons ,scons-python2
      #:scons-flags (list (string-append "prefix=" (assoc-ref %outputs "out")))))
   (home-page "http://mypaint.org")
   (synopsis "A graphics application for digital painters")
   (description "A graphics application for digital painters")
   (license license:gpl2+)))

(package (inherit mypaint))
