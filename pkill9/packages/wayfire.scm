;;FIXME: Need to apply fix upstream to link to wf-config properly when using the bundled one, otherwise ned to wrap with LD_LIBRARY_PATH

(define-module (pkill9 packages wayfire)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system meson)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix utils) ;; for substitute-keyword-arguments
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages documentation) ;; for doxygen, doctest
  #:use-module (gnu packages elf) ;; for patchelf
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages image) ;; for libpng, libjpeg-turbo
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages bash) ;; for use by wrap-program
  #:use-module (gnu packages wm)) ;; for wlroots

(define-public wf-config
  (package
   (name "wf-config")
   (version "0.7.1")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url (string-append "https://github.com/WayfireWM/" name "/"))
                  (commit version)))
            (file-name (git-file-name name version))
            (sha256
             (base32
              "1l1yqhmdpwvzb416i29hcvasisbx2f419nlvyl0q2gaw6ay02d80"))))
   (build-system meson-build-system)
   (native-inputs
    (append (package-native-inputs wlroots)
            `(("pkg-config" ,pkg-config))))
   (inputs
    (append (package-inputs wlroots)
            `(("glm"      ,glm)
              ("libxml2"  ,libxml2)
              ("wlroots"  ,wlroots)
              ("libevdev" ,libevdev))))
   (home-page "https://wayfire.org")
   (synopsis "Config library for Wayfire")
   (description "synopsis")
   (license license:expat)))

(define-public wayfire
  (package
   (name "wayfire")
   (version "0.7.2")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url (string-append "https://github.com/WayfireWM/" name "/"))
                  (recursive? #t)
                  (commit (string-append "v" version))))
            (sha256
             (base32
              "1xalhbjdlslwvmxbc2j6ipa3s7ahlxbpsyshfvbiwwkrpgrn9zw0"))))
   (build-system meson-build-system)
   (native-inputs (list cmake patchelf doxygen doctest pkg-config))
   (inputs (list bash
                 glm
                 wayland
                 wayland-protocols
                 cairo
                 libdrm
                 mesa
                 libinput
                 libxkbcommon
                 xcb-util-renderutil
                 libevdev
                 wlroots
                 libxml2 ;; wf-config, even when providing our own
                 libpng
                 libjpeg-turbo
                 wf-config))
   (arguments
    `(#:configure-flags `(,(string-append "-Dcpp_args=-I"         (assoc-ref %build-inputs "wf-config") "/include/wayfire")
                          ,(string-append "-Dcpp_link_args=-ldl " (assoc-ref %build-inputs "wlroots")   "/lib/libwlroots.so "
                                                                  (assoc-ref %build-inputs "wf-config") "/lib/libwf-config.so"))
      #:phases (modify-phases %standard-phases
                              (add-after 'unpack 'patch-shell-path
                               (lambda* (#:key inputs #:allow-other-keys)
                                 (substitute* "src/meson.build"
                                              (("/bin/sh") (string-append (assoc-ref inputs "bash") "/bin/bash")))
                                 (substitute* "src/core/core.cpp"
                                              (("/bin/sh") (string-append (assoc-ref inputs "bash") "/bin/bash")))))
                              (add-before 'validate-runpath 'fix-runpath
                               (lambda* (#:key outputs #:allow-other-keys)
                                 (system (string-append "patchelf --set-rpath \""
                                                        (assoc-ref outputs "out")
                                                        "/lib:$(patchelf --print-rpath "
                                                        (assoc-ref outputs "out") "/bin/wayfire)\" "
                                                        (assoc-ref outputs "out") "/bin/wayfire"))

                                 (system (string-append "patchelf --set-rpath \""
                                                        (assoc-ref outputs "out")
                                                        "/lib:$(patchelf --print-rpath "
                                                        (assoc-ref outputs "out") "/bin/wayfire)\" "
                                                        (assoc-ref outputs "out") "/lib/wayfire/libwindow-rules.so")))))))
   (home-page "https://wayfire.org")
   (synopsis "Wayland compositor")
   (description "Wayland compositor extendable with plugins.")
   (license license:expat)))

(define-public wf-shell
  ;;FIXME: unbundle gtk-layer-shell and gvc
  (package
   (name "wf-shell")
   (version "0.7.0")
   (source (origin
            (method url-fetch)
            (uri (string-append "https://github.com/WayfireWM/" name
                                "/releases/download/v"          version
                                "/"                             name
                                "-"                             version ".tar.xz"))
            (sha256
             (base32
              "1isybm9lcpxwyf6zh2vzkwrcnw3q7qxm21535g4f08f0l68cd5bl"))))
   (build-system meson-build-system)
   (native-inputs
    (append
     (package-native-inputs wlroots)
     `(("cmake"           ,cmake)
       ("pkg-config" ,pkg-config))))
   (inputs
    (append
     (package-inputs wf-config)
     `(("gtkmm" ,gtkmm)
       ("gobject-introspection" ,gobject-introspection)
       ("pulseaudio" ,pulseaudio)
       ("alsa-lib" ,alsa-lib)
       ("wf-config" ,wf-config))))
   (home-page "https://wayfire.org")
   (synopsis "Panel, dock and background applications for wayfire")
   (description synopsis)
   (license license:expat)))

wayfire
